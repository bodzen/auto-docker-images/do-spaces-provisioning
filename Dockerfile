FROM hashicorp/terraform:light

# Copy code and init terraform
COPY ./tf/ /opt/tf
WORKDIR /opt/tf
ENTRYPOINT ["/bin/terraform", "-v"]
