provider "digitalocean" {
	token = "${var.DO_TOKEN}"
	version = "~> 1.8.0"
	spaces_access_id  = "${var.DO_SPACES_ACCESS_ID}"
	spaces_secret_key = "${var.DO_SPACES_SECRET_KEY}"
}
