resource "digitalocean_spaces_bucket" "fooBOIDbis" {
	name          = "${var.DO_SPACE_NAME}"
	region        = "${var.DO_SPACE_REGION}"
	force_destroy = true
	acl           = "private"
}
