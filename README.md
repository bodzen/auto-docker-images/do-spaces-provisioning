# Terraform module to build a DO Space

![bastion](https://gitlab.com/bodzen/images-stocks/raw/master/spaces/buildings/bastion_01.jpg)

The pipeline will build + push an image containing the terraform code.
This image will be used by two gitlab-ci playbooks:
* [create_tmp_do_space](https://gitlab.com/bodzen-playbooks/terraform/blob/master/create_tmp_do_space.yml)
* [nuke_tmp_do_space](https://gitlab.com/bodzen-playbooks/terraform/blob/master/nuke_tmp_do_space.yml)
